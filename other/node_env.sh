#!/bin/sh

if [ "$NODEJS_HOME" != "$PWD/node-v9.0.0" ]; then
    if [ ! -d "node-v9.0.0" ]; then
        echo "Installing Node.js 9.0.0.."

        wget -c https://nodejs.org/dist/v9.0.0/node-v9.0.0-linux-x64.tar.xz &&
        tar -xf node-v9.0.0-linux-x64.tar.xz &&
        mv node-v9.0.0-linux-x64 node-v9.0.0 &&
        rm node-v9.0.0-linux-x64.tar.xz &&

        echo "Done" || (
            echo "An error ocurred installing Node.js"
            exit
        )
    fi

    NODEJS_HOME="$PWD/node-v9.0.0"
    PATH="$NODEJS_HOME/bin:$PATH"

    echo "Node.js (9.0.0) activated"
fi
